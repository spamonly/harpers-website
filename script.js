function initialize() {
	document.getElementById("starlight").removeAttribute("controls");
	document.getElementById("starlight-outer").childNodes[1].style.display = "block";
	document.getElementById("starlight-outer").style.opacity = 0.5;
	togglecontent(document.getElementById("about"));
	togglecontent(document.getElementById("reel"));
}

function togglecontent(element) {
	if (element.style.display == "none") {
		element.style.display = "block";
	}
	else {
		element.style.display = "none";
	}
}

function togglevid(element) {
	if (element.childNodes[3].paused && ! element.childNodes[3].ended) {
		element.style.opacity = "1";
		element.childNodes[1].style.display = "none";
		element.childNodes[3].play();
	}
	else {
		element.style.opacity = "0.5";
		element.childNodes[1].style.display = "block";
		element.childNodes[3].pause();
	}
}
